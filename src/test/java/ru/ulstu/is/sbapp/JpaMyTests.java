package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.sv.Auto.model.Auto;
import ru.ulstu.is.sbapp.sv.Auto.service.AutoService;
import ru.ulstu.is.sbapp.sv.Category.model.Category;
import ru.ulstu.is.sbapp.sv.Category.service.CategoryService;
import ru.ulstu.is.sbapp.sv.Driver.model.Driver;
import ru.ulstu.is.sbapp.sv.Driver.service.DriverService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@SpringBootTest
public class JpaMyTests {
    private static final Logger log = LoggerFactory.getLogger(JpaMyTests.class);

    @Autowired
    private DriverService driverService;
    @Autowired
    private AutoService autoService;
    @Autowired
    private CategoryService categoryService;

    //StudentTests
    @Test
    void testDriverCreate() {
        driverService.deleteAllDrivers();
        final Driver driver = driverService.addDriver("Иван", "Иванов");
        log.info(driver.toString());
        Assertions.assertNotNull(driver.getId());
    }

    @Test
    void testDriverRead() {
        driverService.deleteAllDrivers();
        final Driver student = driverService.addDriver("Иван", "Иванов");
        log.info(student.toString());
        final Driver findStudent = driverService.findDriver(student.getId());
        log.info(findStudent.toString());
        Assertions.assertEquals(student, findStudent);
    }

    @Test
    void testDriverReadNotFound() {
        driverService.deleteAllDrivers();
        Assertions.assertThrows(EntityNotFoundException.class, () -> driverService.findDriver(-1L));
    }

    @Test
    void testAddingCategoryToDriver(){
        categoryService.deleteAllCategories();
        driverService.deleteAllDrivers();
        Category category = categoryService.addCategory("B");
        Driver driver = driverService.addDriver("Иван", "Иванов");
        driverService.addCategoryDriver(category.getId(), driver.getId());
        driver = driverService.findDriver(driver.getId());
        category = categoryService.findCategory(category.getId());
        Assertions.assertEquals(driver.getCategories().get(0), category);
    }

    @Test
    void testStudentReadAll() {
        driverService.deleteAllDrivers();
        driverService.addDriver("Иван", "Иванов");
        driverService.addDriver("Петр", "Петров");
        final List<Driver> students = driverService.findAllDrivers();
        log.info(students.toString());
        Assertions.assertEquals(students.size(), 2);
    }

    @Test
    void testStudentReadAllEmpty() {
        driverService.deleteAllDrivers();
        final List<Driver> students = driverService.findAllDrivers();
        log.info(students.toString());
        Assertions.assertEquals(students.size(), 0);
    }

    //GroupTests
    @Test
    void testGroupCreate() {
        autoService.deleteAllAuto();
        final Auto auto = autoService.addAuto("mazda", 10);
        log.info(auto.toString());
        Assertions.assertNotNull(auto.getId());
    }

    @Test
    void testGroupRead() {
        autoService.deleteAllAuto();
        final Auto auto = autoService.addAuto("mazda", 10);
        log.info(auto.toString());
        final Auto findGroup = autoService.findAuto(auto.getId());
        log.info(findGroup.toString());
        Assertions.assertEquals(auto, findGroup);
    }

    @Test
    void testGroupReadNotFound() {
        autoService.deleteAllAuto();
        Assertions.assertThrows(EntityNotFoundException.class, () -> autoService.findAuto(-1L));
    }

    @Test
    void testGroupReadAll() {
        autoService.deleteAllAuto();
        autoService.addAuto("mazda", 10);
        autoService.addAuto("forz", 12);
        final List<Auto> autos = autoService.findAllAuto();
        log.info(autos.toString());
        Assertions.assertEquals(autos.size(), 2);
    }

    @Test
    void testGroupReadAllEmpty() {
        autoService.deleteAllAuto();
        final List<Auto> autos = autoService.findAllAuto();
        log.info(autos.toString());
        Assertions.assertEquals(autos.size(), 0);
    }

    //SubjectTests
    @Test
    void testSubjectCreate() {
        categoryService.deleteAllCategories();
        final Category subject = categoryService.addCategory("B");
        log.info(subject.toString());
        Assertions.assertNotNull(subject.getId());
    }

    @Test
    void testSubjectRead() {
        categoryService.deleteAllCategories();
        final Category category = categoryService.addCategory("Интернет программирование");
        log.info(category.toString());
        final Category findSubject = categoryService.findCategory(category.getId());
        log.info(findSubject.toString());
        Assertions.assertEquals(category, findSubject);
    }

    @Test
    void testSubjectReadNotFound() {
        categoryService.deleteAllCategories();
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.findCategory(-1L));
    }

    @Test
    void testSubjectReadAll() {
        categoryService.deleteAllCategories();
        categoryService.addCategory("C");
        categoryService.addCategory("B");
        final List<Category> categories = categoryService.findAllCategories();
        log.info(categories.toString());
        Assertions.assertEquals(categories.size(), 2);
    }

    @Test
    void testSubjectReadAllEmpty() {
        categoryService.deleteAllCategories();
        final List<Category> categories = categoryService.findAllCategories();
        log.info(categories.toString());
        Assertions.assertEquals(categories.size(), 0);
    }
}
