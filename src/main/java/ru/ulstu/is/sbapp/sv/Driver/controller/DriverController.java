package ru.ulstu.is.sbapp.sv.Driver.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.sv.Driver.model.Driver;
import ru.ulstu.is.sbapp.sv.Driver.service.DriverService;

import java.util.List;

public class DriverController {
    private final DriverService driverService;


    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @GetMapping("/{id}")
    public Driver getDriver(@PathVariable Long id) {
        return driverService.findDriver(id);
    }

    @GetMapping("/")
    public List<Driver> getDriver() {
        return driverService.findAllDrivers();
    }

    @PostMapping("/")
    public Driver createDriver(@RequestParam("firstName") String firstName,
                                    @RequestParam("lastName") String lastName) {
        return driverService.addDriver(firstName, lastName);
    }

    @PatchMapping("/{id}")
    public Driver updateDriver(@PathVariable Long id,
                                    @RequestParam("firstName") String firstName,
                                    @RequestParam("lastName") String lastName) {
        return driverService.updateDriver(id, firstName, lastName);
    }

    @DeleteMapping("/{id}")
    public Driver deleteDriver(@PathVariable Long id) {
        return driverService.deleteDriver(id);
    }
}
