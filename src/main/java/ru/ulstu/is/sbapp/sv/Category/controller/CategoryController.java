package ru.ulstu.is.sbapp.sv.Category.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.sv.Category.model.Category;
import ru.ulstu.is.sbapp.sv.Category.service.CategoryService;

import java.util.List;

public class CategoryController {
    private final CategoryService categoryService;


    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/{id}")
    public Category getCategory(@PathVariable Long id) {
        return categoryService.findCategory(id);
    }

    @GetMapping("/")
    public List<Category> getDriver() {
        return categoryService.findAllCategories();
    }

    @PostMapping("/")
    public Category createDriver(@RequestParam("category") String category) {
        return categoryService.addCategory(category);
    }

    @PatchMapping("/{id}")
    public Category updateCategory(@PathVariable Long id,
                               @RequestParam("category") String category) {
        return categoryService.updateCategory(id, category);
    }

    @DeleteMapping("/{id}")
    public Category deleteCategory(@PathVariable Long id) {
        return categoryService.deleteCategory(id);
    }
}
