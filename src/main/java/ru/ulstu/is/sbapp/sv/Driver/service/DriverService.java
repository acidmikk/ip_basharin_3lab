package ru.ulstu.is.sbapp.sv.Driver.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.sv.Category.model.Category;
import ru.ulstu.is.sbapp.sv.Driver.model.Driver;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class DriverService {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Driver addDriver(String firstName, String lastName) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName)) {
            throw new IllegalArgumentException("Driver name is null or empty");
        }
        final Driver driver = new Driver(firstName, lastName);
        em.persist(driver);
        return driver;
    }

    @Transactional(readOnly = true)
    public Driver findDriver(Long id) {
        final Driver driver = em.find(Driver.class, id);
        if (driver == null) {
            throw new EntityNotFoundException(String.format("Driver with id [%s] is not found", id));
        }
        return driver;
    }

    @Transactional(readOnly = true)
    public List<Driver> findAllDrivers() {
        return em.createQuery("select s from Driver s", Driver.class)
                .getResultList();
    }

    @Transactional
    public Driver updateDriver(Long id, String firstName, String lastName) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName)) {
            throw new IllegalArgumentException("Driver name is null or empty");
        }
        final Driver currentDriver = findDriver(id);
        currentDriver.setFirstName(firstName);
        currentDriver.setLastName(lastName);
        return em.merge(currentDriver);
    }

    @Transactional
    public Driver deleteDriver(Long id) {
        final Driver currentDriver = findDriver(id);
        em.remove(currentDriver);
        return currentDriver;
    }

    @Transactional
    public void addCategoryDriver(Long categoryId,Long driverId){
        var category = em.find(Category.class, categoryId);
        var driver = findDriver(driverId);
        driver.addCategory(category);
        em.merge(driver);
    }

    @Transactional
    public void deleteAllDrivers() {
        em.createQuery("delete from Driver").executeUpdate();
    }
}
