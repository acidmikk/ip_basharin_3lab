package ru.ulstu.is.sbapp.sv.Auto.model;

import org.springframework.jmx.export.naming.IdentityNamingStrategy;
import ru.ulstu.is.sbapp.sv.Category.model.Category;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "auto")
public class Auto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String Name;
    @Column
    private Integer cost1km;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Auto() {
    }

    public Auto(String Name, Integer cost1km) {
        this.Name = Name;
        this.cost1km = cost1km;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Integer getCost1km() { return cost1km; }

    public void setCost1km(Integer cost1km) {
        this.cost1km = cost1km;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auto auto = (Auto) o;
        return Objects.equals(id, auto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Auto{" +
                "id=" + id +
                ", Name='" + Name + '\'' +
                ", cost1km='" + cost1km + '\'' +
                '}';
    }
}