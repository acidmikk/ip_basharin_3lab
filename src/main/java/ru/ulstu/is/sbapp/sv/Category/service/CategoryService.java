package ru.ulstu.is.sbapp.sv.Category.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.sv.Category.model.Category;
import ru.ulstu.is.sbapp.sv.Driver.model.Driver;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class CategoryService {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Category addCategory(String category) {
        if (!StringUtils.hasText(category)) {
            throw new IllegalArgumentException("Category name is null or empty");
        }
        final Category category1 = new Category(category);
        em.persist(category1);
        return category1;
    }

    @Transactional(readOnly = true)
    public Category findCategory(Long id) {
        final Category category = em.find(Category.class, id);
        if (category == null) {
            throw new EntityNotFoundException(String.format("Category with id [%s] is not found", id));
        }
        return category;
    }

    @Transactional(readOnly = true)
    public List<Category> findAllCategories() {
        return em.createQuery("select s from Category s", Category.class)
                .getResultList();
    }

    @Transactional
    public Category updateCategory(Long id, String category) {
        if (!StringUtils.hasText(category)) {
            throw new IllegalArgumentException("Category name is null or empty");
        }
        final Category currentCategory = findCategory(id);
        currentCategory.setCategory(category);
        return em.merge(currentCategory);
    }

    @Transactional
    public Category deleteCategory(Long id) {
        final Category currentCategory = findCategory(id);
        em.remove(currentCategory);
        return currentCategory;
    }

    @Transactional
    public void deleteAllCategories() {
        em.createQuery("delete from Category").executeUpdate();
    }

}
