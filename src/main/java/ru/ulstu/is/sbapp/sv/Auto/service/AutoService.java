package ru.ulstu.is.sbapp.sv.Auto.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.sv.Auto.model.Auto;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;
@Service
public class AutoService {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Auto addAuto(String Name, Integer cost1km) {
        if (!StringUtils.hasText(Name)) {
            throw new IllegalArgumentException("Auto name is null or empty");
        }
        final Auto auto = new Auto(Name, cost1km);
        em.persist(auto);
        return auto;
    }

    @Transactional(readOnly = true)
    public Auto findAuto(Long id) {
        final Auto driver = em.find(Auto.class, id);
        if (driver == null) {
            throw new EntityNotFoundException(String.format("Auto with id [%s] is not found", id));
        }
        return driver;
    }

    @Transactional(readOnly = true)
    public List<Auto> findAllAuto() {
        return em.createQuery("select s from Auto s", Auto.class)
                .getResultList();
    }

    @Transactional
    public Auto updateAuto(Long id, String Name, Integer cost1km) {
        if (!StringUtils.hasText(Name)) {
            throw new IllegalArgumentException("Auto name is null or empty");
        }
        final Auto currentAuto = findAuto(id);
        currentAuto.setName(Name);
        currentAuto.setCost1km(cost1km);
        return em.merge(currentAuto);
    }

    @Transactional
    public Auto deleteAuto(Long id) {
        final Auto currentAuto = findAuto(id);
        em.remove(currentAuto);
        return currentAuto;
    }

    @Transactional
    public void deleteAllAuto() {
        em.createQuery("delete from Auto").executeUpdate();
    }

}
