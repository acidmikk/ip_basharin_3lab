package ru.ulstu.is.sbapp.sv.Category.model;

import ru.ulstu.is.sbapp.sv.Auto.model.Auto;
import ru.ulstu.is.sbapp.sv.Category.model.Category;
import ru.ulstu.is.sbapp.sv.Driver.model.Driver;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column()
    private String category;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "categories")
    private List<Driver> drivers = new ArrayList<>();

    @OneToMany(mappedBy = "category", orphanRemoval = true)
    private List<Auto> autos;

    public List<Auto> getAutos() {
        return autos;
    }

    public void setAutos(List<Auto> autos) {
        this.autos = autos;
    }

    public List<Driver> getDrivers() { return drivers; }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }


    public Category() {
    }

    public Category(String category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(id, category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category='" + category + '\'' +
                '}';
    }
}